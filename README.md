# Intelligent-Chatbot
Intelligent Chatbot using Deep Neural Network [Python, TensorFlow, Seq2Seq, PyQT]	[Jan – May 18]
-	Developed chatbot using encoder and decoder based Sequence-to-Sequence (Seq2Seq) model from Google’s Neural Machine Translation (NMT) module and Cornell Movie Subtitle Corpus. 
-	Seq2Seq architecture built on Recurrent Neural Network was optimized with bidirectional LSTM cells.
-	Enhanced chatbot performance by applying Neural Attention Mechanism and Beam Search.
-	Developed backend using Python and front-end using Python and PyQT.
